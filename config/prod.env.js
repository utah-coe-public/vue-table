'use strict'
module.exports = {
  NODE_ENV: '"production"',
  APP_NAME: 'VueTable',
  APP_FILENAME: 'vue-table',
  APP_VERSION: '1.0',
  externals: {
    "jquery": "jQuery",  
    "bootstrap": "bootstrap",
    "lodash": "lodash",
    "fontawesome": "fontawesome"
    }
}
