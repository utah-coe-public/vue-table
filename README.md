# vue-table

> Render a dynamic html table based on supplied data.  Contains search bar (fuzzy search using fuzejs), pagination, column sort.  It can load data from supplied 
array of objects or from a url.

## Using vue-table:

Example with objects:
---
```
<vue-table :bootstrap="bootstrap_styles" 
            :table_headers="table_headers" 
            v-model="table_data" 
            :disable_filters="true"
            :disable_exports="false"
            :disable_results_count="false"
            :disable_search="false">
    <template slot-scope="{header, row}">
        <template v-for="action in row[header.field]">
            <span v-on:click="action.onclick" v-html="action.html"></span>
        </template>
    </template>
</vue-table>
```

Example with pagenator:
---

```
    <vue-table :bootstrap=bootstrap_styles 
        :table_headers=table_headers
        v-model="pagenatorObject" 
        :pagenator="pagenatorObject" 
        :disable_filters=true 
        :disable_exports=false 
        :disable_results_count=false 
        :disable_search=false 
        :mutator_function="mutateData" 
        :extra_params="extra_params"
        v-on:data-change="tableDataChange"
        >
        <!-- template slot for header and row -->
        <template slot-scope="{header, row}">
            <template v-for="action in row[header.field]">
                <span v-on:click="action.onclick" v-html="action.html"></span>
            </template>
        </template>
    </vue-table>
```

Pagenator Model Fields:
---

```
pagenatorModel = {
    data:           [], //objects for each row
    last_page:      6,
    path:           "/dist/data.json",
    total:          600
};
```

Pagenator Backend:
---

Web backend for pagenator will get requests with the following Query String Parameters:

* page
* num_items
* search
* sort_by
* sort_order (1 or -1 for ascending or descending)

For example, navigating to page 2 of data, would trigger a request to a URL like so:

```
http://myserver.example.com/mypath?page=2&num_items=10&search=dar&sort_by=email&sort_order=1
```

Header Definitions:
---

Sample Header Defnition:
```
var table_headers = [{name: "", field: "actions", type: "slot"},
                    {name: "Name", field: "scholarship_name", type: "html"},
                    {name: "Description", field: "scholarship_description", type: "html"},
                    {name: "Balance", field: "balance", type: "number"},
                    {name: "Department", field: "department", type: "html"},
                    {name: "Comments", field: "comments", type: "html"},
                    {name: "Amount", field: "award_amount", type: "html"},
                    {name: "Quantity", field: "quantity", type: "html"}];
```

Header Types:
---
* slot -> Allows for embedding onClick actions that will call parent Vue instance.  Sample usage: 
  ```
  [{'html': '<a href="/scholarship-manager/scholarship/' + scholarship._id + '" class="btn btn-sm btn-success d-block w-100"><span class="fas fa-eye" width="60"></span></a>', 'onclick': () => {/* noop */}}];
  ``` 
* html -> renders HTML
* text -> renders text
* number -> renders text but sorts by number
* datetime -> renders values as formatted datetime string as provided by **datetime_format** setting or the default `'M/D/Y h:mm a'`

Settings:
---
The following settings are available to set on your components

| Setting | Default | Description |
| ------ | ------ | ------ |
| num_per_page_selected | 5 | number of rows to display on load if not using saved settings from UI |
| row_class_field | null | a class to add to each row of the rendered HTML for purposes of styling and such |
| enable_settings_ui | false |  enable the table settings UI for saving state of table on page reloads using local storage |
| csv_field_blacklist | [] |  exclude fields from the generated csv file when saving data to csv |
| csv_download_endpoint | null |  csv download override utilizing a download endpoint on the server |
| datetime_format | 'M/D/Y h:mm a' |  format string for formating values of field type **datetime**. utlizes moment.js |
| selectable_rows | false |  if true, shows a bulk selectable checkbox in header and associated checkboxes for each row.  false, doesn't show checkboxes |

**Example using settings: **
```
<vue-table :bootstrap="true" :disable_filters="true" :table_data="transactions_table_data" :table_headers="transactions_table_headers" :settings="{num_per_page_selected: 10, enable_settings_ui: true}">
</vue-table>
```

Emitters:
---

| Name | Description |
| ------ | ------ |
| rowclick | emits the underlying object describing the row that was clicked |
| data-change | emits with no data, just a signal something changed |
| selected-rows-change | emits current list of underlying row objects that are selected via the selectable checkbox whenever the list changes|
| input | emits the tabledata anytime it's changed via sorting, new pagenation fetch, etc...|


Tips & Tricks:
---

If you need to call for fresh results of current paginator (say after deleting some records or something)
name your child via vue $ref's (https://vuejs.org/v2/api/#ref).  and have access to any function

HTML: 
```
<vue-table  :pagenator="info_requests" 
            :table_headers="table_headers" 
            :table_primary_key="table_primary_key" 
            v-model="info_requests"  
            ref="infoRequestsTable">
</vue-table>
```
JS (in parent component): 
```
this.$refs.infoRequestsTable.fetchNewPagenator();
```


## Testing

'npm test' should run nightwatch javascript tests using chromedriver

After building and starting a server with 'npm run build; npm run server', and starting a
node-based server to act as a backend ('node src/pagenator-backend.js'), you can visit
http://localhost:8081/dist/pagination-test.html to test out loading data from a remote 
backend.  This process is pretty clumsy, so future improvements could be to make this testing
smoother.


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

## Historical Notes


*  *Prior to pagenation, there was no v-model handlining.  Table data was passed in via **table_data** prop.  That prop is still accepted, but will cause warnings when sorting happens internal to vue-table.  It is preferred to use v-model to pass in table data whether using pagenation or not.*
