// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'

import bootstrap from 'bootstrap';
import fontawesome from 'font-awesome/less/font-awesome.less';
import 'fuse.js';
import _ from 'lodash';
import FileSaver from 'file-saver/FileSaver.js';
import json2csv from 'json2csv';
import VueTable from './components/VueTable.vue'

import VueResource from 'vue-resource';

global.jQuery = require('jquery');
var $ = global.jQuery;
window.$ = $;
global.Fuse = require('fuse.js');
global.json2csv = json2csv;
global.FileSaver = FileSaver;


export default VueTable
export {VueTable}