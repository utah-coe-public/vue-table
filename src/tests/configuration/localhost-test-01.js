//test page number is retained in localstorage and restored when revisiting table
module.exports = {
  // '@disabled': true,
  'step one: navigate to page' : function (browser) {
      browser
        // .pause(3000)
        .url('http://localhost:8081/dist/')
        .waitForElementVisible('body', 1000)
        .assert.containsText('body', 'Senator');
    },
  
    'step two: click input' : function (browser) {
      browser
        .assert.cssClassNotPresent('ul.pagination li:nth-child(6)', 'active') //the "4" page is not active
        .click('ul.pagination li:nth-child(6) span') //the "4" page in pagination
        .assert.cssClassPresent('ul.pagination li:nth-child(6)', 'active'); //the "4" page is active

        // .pause(1000)
        // .assert.containsText('body', 'Senator')
        // .end();
    },
  
    'step three: navigate away and back' : function (browser) {
      browser
      .url('https://google.com')
      .waitForElementVisible('body', 1000)
      .url('http://localhost:8081/dist/')
      .waitForElementVisible('body', 1000)
      .assert.cssClassPresent('ul.pagination li:nth-child(6)', 'active'); //the "4" page is active
    }
};


