//test num_per_page is retained in localstorage and restored when revisiting table
module.exports = {
  // '@disabled': true,
  'step one: navigate to page' : function (browser) {
      browser
        .pause(3000)
        .url('http://localhost:8081/dist/')
        .waitForElementVisible('body', 1000)
        .assert.containsText('body', 'Senator');
  },
  
  'step two: fill search input' : function (browser) {
    browser
      .assert.value('select', "10")
      .setValue('select', '25');
  },

  'step three: navigate away and back' : function (browser) {
    browser
    .url('https://google.com')
    .waitForElementVisible('body', 1000)
    .url('http://localhost:8081/dist/')
    .waitForElementVisible('body', 1000)
    .assert.value('select', "25");
  }
};


