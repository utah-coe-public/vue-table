//test num_per_page is retained in localstorage and restored when revisiting table
module.exports = {
    '@disabled': true,
    'step one: navigate to page' : function (browser) {
      browser
        .url('http://localhost:8081/dist/')
        .waitForElementVisible('body', 1000)
        .assert.containsText('body', 'Senator');
    },
  
    'step two: fill search input' : function (browser) {
      browser
        .assert.value('select', "10")
        .setValue('select', '25');
    }
  
};


