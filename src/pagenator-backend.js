/*
Example backend for vue-table
*/

const express = require('express');
const app = express();
const port = 3000;
const exampleData = require('./tests/pagination/data.json');
var cors = require('cors');

app.get('/', cors(), (req, res) => 
{
    var page = 1, num_items = 10, search = "", sort_by = false, sort_order = 1;
    if (req.query.page) {
        page = parseInt(req.query.page);
    }
    page--;
    if (page < 0) {
        page = 0;
    }

    if (req.query.num_items) {
        num_items = parseInt(req.query.num_items);
    }
    if (req.query.search) {
        search = req.query.search;
    }
    if (req.query.sort_by) {
        sort_by = req.query.sort_by;
    }
    if (req.query.sort_order) {
        sort_order = parseInt(req.query.sort_order);
    }

    var parameters = {
        num_items,
        search,
        sort_by,
        sort_order,
        page
    }

    console.log("Parameters:");
    console.log(parameters);

    var returnedData = exampleData;
    
    //do search
    //for testing, only going to search on name
    if (search) {
        returnedData = returnedData.filter(d => {
            return d.display_name && d.display_name.indexOf(search) !== -1
        });
    }

    //do sort
    if (sort_by) {
        returnedData = returnedData.sort( (a,b) => {
            var result = 1;
            if (a[sort_by] > b[sort_by]) {
                result = 1;
            } else if (a[sort_by] < b[sort_by]) {
                result = -1;
            } else {
                result = 0;
            }
            return result * sort_order;
        });
    }


    var pagenatorModel = {
        data:           [],
        last_page:      Math.ceil(returnedData.length / num_items),
        path:           "http://localhost:3000/",
        total:          returnedData.length
    };

    //return page
    var start_index = page * num_items;
    var end_index   = start_index + num_items;
    returnedData    = returnedData.slice(start_index, end_index);

    pagenatorModel.data = returnedData;

    res.json(pagenatorModel);
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

